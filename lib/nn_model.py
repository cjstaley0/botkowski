import logging
import numpy
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.models import Sequential
from keras.utils import np_utils
import nlp_utils

POS_SET = ['#', '$', ',', '.', ':', 'CC', 'CD', 'DT', 'EX', 'FW', 'IN', 'JJ', 'JJR', 'JJS', 'LB', 'LS', 'MD', 'NN',
           'NNP', 'NNPS', 'NNS', 'PDT', 'POS', 'PRP', 'PRP$', 'RB', 'RBR', 'RBS', 'RP', 'SYM', 'TO', 'UH', 'VB', 'VBD',
           'VBG', 'VBN', 'VBP', 'VBZ', 'WDT', 'WP', 'WP$', 'WRB']


class Model:
    def __init__(self, lines, text_generator=None, weights_name='../resources/weights', min_length=20):
        self.lines = lines
        self.text_generator = text_generator
        self.weights_name = weights_name
        self.pos_list = []
        self.min_length = min_length
        self.token_list = []
        self.pos_to_int = {}
        self.int_to_pos = {}
        self.dataX = []
        self.X = []
        self.y = []
        self.model = None

    def ingest_tokens(self):
        logging.info('Processing ' + str(len(self.lines)) + ' lines of text')

        for line in self.lines:
            if len(line) > self.min_length:
                self.token_list += \
                    nlp_utils.get_tokens(line, add_bookends=False) + \
                    [nlp_utils.Token('\n', 'LB')]

        self.pos_list = map(lambda x: x.pos, self.token_list)

        self.pos_to_int = dict((c, i) for i, c in enumerate(POS_SET))
        self.int_to_pos = dict((i, c) for i, c in enumerate(POS_SET))

        # summarize the loaded data
        n_tokens = len(self.pos_list)
        n_vocab = len(POS_SET)
        logging.info('Total tagged tokens: ' + str(n_tokens))
        logging.info('Total tags: ' + str(n_vocab))

    def get_model(self, layers=None, seq_length=25):
        # prepare the dataset of input to output pairs encoded as integers
        if layers is None:
            layers = [128, 128]
        dataY = []
        for i in range(0, len(self.pos_list) - seq_length, 1):
            seq_in = self.pos_list[i:i + seq_length]
            seq_out = self.pos_list[i + seq_length]
            self.dataX.append([self.pos_to_int.get(pos, 15) for pos in seq_in])
            dataY.append(self.pos_to_int.get(seq_out, 15))
        n_patterns = len(self.dataX)
        logging.info("Total Patterns: " + str(n_patterns))

        # reshape X to be [samples, time steps, features]
        self.X = numpy.reshape(self.dataX, (n_patterns, seq_length, 1))
        # normalize
        self.X = self.X / float(len(self.pos_to_int))
        # one hot encode the output variable
        self.y = np_utils.to_categorical(dataY)

        # define the LSTM model
        self.model = Sequential()
        if len(layers) == 1:
            self.model.add(LSTM(layers[0], input_shape=(self.X.shape[1], self.X.shape[2])))
            self.model.add(Dropout(0.2))
        else:
            self.model.add(LSTM(layers[0], input_shape=(self.X.shape[1], self.X.shape[2]), return_sequences=True))
            self.model.add(Dropout(0.2))
            for layer in layers[1:-1]:
                self.model.add(LSTM(layer, return_sequences=True))
                self.model.add((Dropout(0.2)))
            self.model.add(LSTM(layers[-1]))
            self.model.add(Dropout(0.2))
        self.model.add(Dense(len(self.pos_to_int), activation='softmax'))

    def load_weights(self, path):
        self.model.load_weights(path)

    def compile(self):
        self.model.compile(loss='categorical_crossentropy', optimizer='adam')

    def fit(self):
        # define the checkpoint
        filepath = self.weights_name + '-{epoch:02d}-{loss:.4f}.hdf5'
        checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
        callbacks_list = [checkpoint]
        # fit the model
        self.model.fit(self.X, self.y, epochs=50, batch_size=64, callbacks=callbacks_list)
