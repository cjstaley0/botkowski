import argparse
import codecs
import nn_model


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--text')
    parser.add_argument('-l1', '--layer1', type=int, default=32)
    parser.add_argument('-l2', '--layer2', type=int, default=256)
    parser.add_argument('-s', '--seq_length', type=int, default=20)
    args = parser.parse_args()
    hyper_params = ([args.layer1, args.layer2], args.seq_length)

    text = args.text
    print 'Processing {}'.format(text)
    with codecs.open(text, 'r', encoding='utf8') as text_input:
        lines = text_input.readlines()
    model = nn_model.Model(lines)
    model.ingest_tokens()
    model.get_model(hyper_params[0], hyper_params[1])
    model.compile()
    model.fit()


if __name__ == '__main__':
    main()
