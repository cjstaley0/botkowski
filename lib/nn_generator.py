# coding=utf-8
import logging
import numpy
import nlp_utils


class NNGenerator:
    def __init__(self, text_file, weights_file):
        self.text_file = text_file
        self.weights_file = weights_file

    def create_pos_map(self, token_list):
        pos_map = {}
        for token in token_list:
            if token.pos in pos_map:
                pos_map[token.pos].append(token.text)
            else:
                pos_map[token.pos] = [token.text]
        return pos_map

    def get_start_pattern(self, model, stop='LB'):
        start = numpy.random.randint(0, len(model.dataX) - 1)
        while model.int_to_pos[model.dataX[start][-1]] != stop:
            start += 1
            if start >= len(model.dataX):
                start = 0
        start_pattern = model.dataX[start]
        logging.info('Seed:')
        for line in ' '.join([model.int_to_pos[value].replace('LB', '\n') for value in start_pattern]).split('\n'):
            logging.info(line)
        return start_pattern

    def process_text(self, pos, prev_pos, current_bigram, next_text, result_text):
        if pos in ['VBD', 'VBN', 'VBP', 'VBZ'] and current_bigram[1] == 'me':
            current_bigram = (current_bigram[0], 'i')
            result_text = result_text.rstrip('me') + 'i'
            logging.info('me to i')
        if pos == 'DT' and prev_pos == 'DT':
            current_bigram = (current_bigram[0], 'all')
            result_text = result_text.rstrip(current_bigram[1]) + 'all'
            logging.info('the to all')
        if prev_pos in ['VBD', 'VBN', 'VBP', 'VBZ', 'VBG'] and next_text.lower() == 'i':
            next_text = 'me'
            logging.info('i to me')
        if current_bigram[1] != 'i' and next_text.lower() == 'am':
            next_text = 'is'
            logging.info('am to is')
        if current_bigram[1] == 'i' and next_text.lower() == 'is':
            next_text = 'am'
            logging.info('is to am')
        if prev_pos not in ['.', 'LB'] and pos[0] not in ['N', 'J'] and next_text != 'I' and next_text != next_text.lower():
            if next_text != next_text.upper():
                next_text = next_text.lower()
        if next_text.lower() in ['n\'t', u'n\u2019t'] and current_bigram[1].lower() not in ['is', 'were', 'has', 'did', 'ai', 'should', 'have', 'could', 'are', 'would']:
            next_text = 'not'
        if next_text.lower() in ['\'ve', u'\u2019ve'] and current_bigram[1].lower() not in ['i', 'you', 'they', 'we', 'should', 'would', 'might', 'could']:
            next_text = 'have'
        if next_text.lower() in ['\'ll', u'\u2019ll'] and current_bigram[1].lower() not in ['i', 'you', 'they', 'we', 'he', 'she', 'it', 'that', 'there']:
            next_text = 'will'
        if next_text.lower() in ['\'s', u'\u2019s'] and current_bigram[1].lower() not in ['he', 'she', 'it', 'there', 'here']:
            next_text = 'is'
        if current_bigram[1] == 'ca' and next_text.lower() not in ['n\'t', u'n\u2019t']:
            current_bigram = (current_bigram[0], 'can')
            result_text = result_text + 'n'
        if pos in ['NNS', 'NNPS'] and current_bigram[1] in ['a', 'an']:
            result_text = result_text.rstrip(current_bigram[1]) + 'the'
            current_bigram = (current_bigram[0], 'the')
            logging.info('a to the')

        result_text += nlp_utils.get_spacer(current_bigram[1], next_text) + next_text
        current_bigram = (current_bigram[1], next_text.lower())

        return current_bigram, result_text
