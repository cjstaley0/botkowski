import argparse
import logging
from text_gen import TextGenerator
from nn_generator import NNGenerator


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--name')  # filename
    parser.add_argument('-w', '--weights')  # weights file
    parser.add_argument('-l', '--lines', type=int, default=10)

    # Values below must be the same values used in creating the weights file
    parser.add_argument('-l1', '--layer1', type=int, default=32)
    parser.add_argument('-l2', '--layer2', type=int, default=256)
    parser.add_argument('-s', '--seq_length', type=int, default=20)
    args = parser.parse_args()

    logging.basicConfig(
        format='%(asctime)s %(levelname)-4s %(message)s',
        filename='/var/tmp/poetry.log',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    nn_gen = NNGenerator(args.name, args.weights)
    text_gen = TextGenerator(nn_gen, args.lines, [args.layer1, args.layer2], args.seq_length)
    poem = text_gen.get_text()
    print poem

if __name__ == '__main__':
    main()
