import logging
import re
import numpy
import codecs
import nn_model
import nlp_utils


class TextGenerator:
    def __init__(self, nn_gen, lines, layers, seq_length):
        self.nn_gen = nn_gen
        self.lines = lines
        self.layers = layers
        self.seq_length = seq_length

    def text_gen(self, model):
        i = 0
        pos = 'LB'
        pos_map = self.nn_gen.create_pos_map(model.token_list)
        bigrams, trigrams = nlp_utils.create_grams(model.token_list)
        current_bigram = ('.', '\n')
        result_text = ''
        pos_result = ''
        start_recording = False
        start_count = 0
        pattern = self.nn_gen.get_start_pattern(model)

        while i < self.lines:
            x = numpy.reshape(pattern, (1, len(pattern), 1))
            x = x / float(len(model.pos_to_int))
            prediction = model.model.predict(x, verbose=0)
            index = numpy.argmax(prediction)
            prev_pos = pos
            pos = model.int_to_pos[index]
            pattern.append(index)
            pattern = pattern[1:len(pattern)]
            if not start_recording:
                start_count += 1
                logging.info('Checking to see if we can start with ' + prev_pos + ':' + pos)
                if prev_pos == 'LB' and pos not in ['CC', 'POS', 'RB', 'TO', 'VBD', 'VBN', 'VBP', 'VBZ', 'WDT']:
                    start_recording = True
                elif start_count > 20:
                    logging.warn('Couldnt find a starting pattern. Getting new seed.')
                    pattern = self.nn_gen.get_start_pattern(model)
                    start_count = 0

            if start_recording:
                text_list = pos_map[pos]
                next_text = nlp_utils.get_pos_text(bigrams, current_bigram, text_list)
                current_bigram, result_text = self.nn_gen.process_text(pos, prev_pos, current_bigram, next_text, result_text)
                pos_result += (pos + ' ').replace('LB ', '\n')
                if pos == 'LB':
                    i += 1
                    logging.info('finished line {}'.format(i))

        logging.info('Generated Pattern:')
        for line in pos_result.split('\n'):
            logging.info(line)

        if re.match(r'[-;:,]+', current_bigram[0]):
            result_text = result_text.rstrip(current_bigram[0] + '\n') + '.'
        else:
            result_text = result_text.rstrip('\n')
        return result_text

    def get_text(self):
        with codecs.open(self.nn_gen.text_file, 'r', encoding='utf8') as text_input:
            lines = text_input.readlines()
        model = nn_model.Model(lines)

        model.ingest_tokens()
        model.get_model(self.layers, self.seq_length)
        model.model.load_weights(self.nn_gen.weights_file)
        model.compile()

        poem = self.text_gen(model)
        return poem

