# -*- coding: utf-8 -*-
import logging
import re
import requests
import json
import itertools
from random import randint


class Token:
    def __init__(self, text, pos):
        self.text = text
        self.pos = pos


def get_tokens(text, add_bookends=True):
    try:
        request = requests.post(
            'http://localhost:9000',
            params={'properties': '{"annotators": "tokenize, pos", "outputFormat": "json"}'},
            data=text.encode('utf-8'),
            headers={'Connection': 'close'}
        )
        sentences = json.loads(request.text, strict=False)['sentences']
        token_info = []
        for sentence in sentences:
            tokens = sentence['tokens']
            token_info += map(lambda x: Token(x['originalText'], x['pos']), tokens)
        filtered_tokens = filter(lambda x: x.text not in ['\'', '"', '[', ']', '(', ')', '`', u'\u2019', u'\u2018', u'\u201c', u'\u201d', '{', '}'], token_info)
        if add_bookends and filtered_tokens:
            filtered_tokens = [Token('bgnbgnbgnbgn', 'BGN')] + filtered_tokens + [Token('endendendend', 'END')]
        return filtered_tokens
    except Exception as e:
        logging.warn('Could not process text ' + text + ' with exception ' + str(e))
        return []


def create_grams(tokens):
    tokens_lc = map(lambda x: x.text.lower(), tokens)
    bigrams = zip(tokens_lc, tokens[1:]) + [(tokens_lc[-1], tokens[0])]
    trigrams = zip(tokens_lc, tokens_lc[1:], tokens[2:]) + [(tokens_lc[-2], tokens_lc[-1], tokens[0]), (tokens_lc[-1], tokens_lc[0], tokens[1])]
    bigram_dict = {k: map(lambda x: x[1], v) for (k, v) in itertools.groupby(sorted(bigrams), lambda x: x[0])}
    trigram_dict = {k: map(lambda x: x[2], v) for (k, v) in itertools.groupby(sorted(trigrams), lambda x: (x[0], x[1]))}

    return bigram_dict, trigram_dict


def get_pos_text(bigrams, current_bigram, text_list):
    bigram_texts = map(lambda x: x.text, bigrams.get(current_bigram[1], []))
    if text_list:
        bigram_filtered = filter(lambda x: x in bigram_texts, text_list)
        if bigram_filtered:
            text_list = bigram_filtered
    else:
        text_list = bigram_texts

    text = text_list[randint(0, len(text_list) - 1)]
    return text


def get_spacer(prev_text, text):
    spacer = ' ' if not re.match(r'^([.!,;:%?][.!?]*)$', text) \
                    and '\'' not in text[:-1] \
                    and u'\u2019' not in text[:-1] \
                    and not (prev_text == u'gon' and text == u'na') \
                    and not (prev_text == u'wan' and text == u'na') \
                    and not (prev_text == u'y\'' and text == u'all') \
                    and not (prev_text == u'y\u2019' and text == u'all') \
                    and not (prev_text == u'lem' and text == u'me') \
                    and not prev_text == u'$' \
                    and not prev_text == u'#' \
                    and not (prev_text == u'-' and text == u'>') \
                    and not (prev_text == '__emo__' and text == '__emo__') \
                    and not prev_text == '\n' \
                    and not text == '\n' \
                    else ''
    return spacer
