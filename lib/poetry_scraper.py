import urllib3.contrib.pyopenssl
from bs4 import BeautifulSoup
import codecs
import argparse
import certifi


class BS:
    def __init__(self, http):
        self.http = http

    def make_soup(self, url):
        response = self.http.request('GET', url)
        result = response.data
        soup = BeautifulSoup(result, 'html.parser')
        return soup


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--number', type=int)
    args = parser.parse_args()
    poem_limit = args.number
    poem_count = 0
    poet_count = 0
    url = 'https://www.poemhunter.com'
    poet_page = '/p/t/l.asp?l=Top500&p='

    urllib3.contrib.pyopenssl.inject_into_urllib3()
    http = http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
    bs = BS(http)
    with codecs.open('../resources/poems2.txt', 'w', encoding='utf-8') as input_file:
        for page in range(1, 11):
            print 'starting page {}'.format(page)
            soup = bs.make_soup('{}{}{}'.format(url, poet_page, page))
            poet_list = soup.find_all('ol', {'class': 'poets-grid'})[0].contents
            poet_list = [poet.find('a')['href'] for poet in poet_list if poet.find('a') != -1]
            for poet in poet_list:
                print 'starting poet: {}'.format(poet)
                poet_count += 1
                soup = bs.make_soup('{}{}/poems'.format(url, poet))
                poems = soup.find_all('table', {'class': 'poems'})[0].find('tbody').contents
                try:
                    poems = [poem.find('a')['href'] for poem in poems if poem.find('a') and poem.find('a') != -1]
                except Exception as e:
                    print 'got error {} for poet {}'.format(str(e), poet)
                for poem in poems:
                    print 'starting poem {}'.format(poem)
                    poem_count += 1
                    soup = bs.make_soup('{}{}'.format(url, poem))
                    try:
                        poem_text = soup.find_all('div', {'class': 'KonaBody'})[0].find('p').contents
                    except Exception as e:
                        print 'got error {} in poem {}'.format(str(e), poem)
                    try:
                        poem_lines = [line.string.strip() for line in poem_text if line.string]
                        for line in poem_lines:
                            if line:
                                input_file.write(line + u'\n')
                    except Exception as e:
                        print 'got error {} in {}'.format(str(e), poem)
                    if poet_count >= poem_limit:
                        exit(0)

    print 'scraped {} poems from {} poets'.format(poem_count, poet_count)


if __name__ == '__main__':
    main()
