#!/usr/bin/env bash

cd /usr/local/bin/stanford-corenlp-full-2017-06-09
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -serverProperties tagger.conf -port 9000 -timeout 15000 -encoding utf-8