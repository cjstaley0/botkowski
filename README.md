**Botkowski Poetry Generator**

There are three parts to the generator:
1. `poetry_scraper.py`

This will collect poems to use as a basis for the new poems. 

*Arguments*
* `-n` number of poems to collect. The more lines, the better it will perform, however more than about 10K lines will cause it to be quite slow. Results will be written to `resources/poems.txt`.

2. `train_nn.py`

This trains the LSTM neural network on the parts of speech patterns in the poems. The POS tagging relies on Stanford CoreNLP. Use the `start_nlp.sh` script to start it up once it is installed. The resulting weights files (50 of them) will be writted to the `resources` directory.

*Arguments*
* `-t` The location of the file created in step 1 or another file of your choosing. You can use the `resources/poems.txt` that is provided
* `-l1` size of layer 1. Default is 32
* `-l2` size of layer 2. Default is 256
* `-s` sequence length. Default is 20 

3. `run_botkowski.py`

This generates the poems. Stanford CoreNLP wil need to be running for this as well.

*Arguments*
* `-n` The location of the text file created in step 1.
* `-w` The location of the weights file created in step 2. You can use `resources/weights.hdf5` as well.
* `-l` The number of lines of poetry to output.
* `-l1` `-l2` `-s` These values must match the values used in creating the weights file. Defaults are the same as above.

